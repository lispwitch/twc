#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* If it's good enough for Pike and co it's good enough for us */
#define uchar unsigned char
#define ulong unsigned long
#define LONGOPT(shortopt, longopt, arg) \
        ((strcmp(shortopt, arg)) == 0 || (strcmp(longopt, arg)) == 0)
#define ASCIIMAX UCHAR_MAX+1


#define HELPSTRING "twc -- tiny word count\n" \
                   "Usage: twc OPTION <files ...>\n" \
                   "Options:\n" \
                   "\t-c, --bytes\n\t\t\tPrint the character/byte count\n" \
                   "\t-w, --words\n\t\t\tPrint the word count\n" \
                   "\t-l, --lines\n\t\t\tPrint the line count\n"

enum equclasses_e {
	EQ_END,
	EQ_BYTE,
	EQ_NEWLINE,
	EQ_WSPACE,
	EQ_NUMCLASSES
};

enum states_e {
	S_END,
	S_BYTE,
	S_ISWORD,
	S_SPACE,
	S_NUMSTATES
};

enum results_e {
	RESULTS_ALL,
	RESULTS_BYTES,
	RESULTS_WORDS,
	RESULTS_LINES
};

typedef struct {
	uchar *s;
	ulong bytes, words, newlines;
} file_stats;


static int equclass[ASCIIMAX] = { 0 };
static int states[S_NUMSTATES][EQ_NUMCLASSES] = { { 0 } };

static int count_newlines[EQ_NUMCLASSES] = { 0 };
static int count_bytes[S_NUMSTATES]      = { 0 };
static int count_words[S_NUMSTATES]      = { 0 };


static uchar *readbuffer(char *path)
{
	struct stat st;
	uchar *s = NULL;
	int fd = -1;
	size_t size = 0;

	/* Grab size of file */
	if ((stat(path, &st)) < 0) {
		perror(path);
		goto _throw;
	}
	
	size = st.st_size;
	if (size == 0) {
		printf("%s is empty.\n", path);
		goto _throw;
	}

	if (!(s = calloc(size, 1))) {
		perror("calloc");
		goto _throw;
	}

	fd = open(path, O_RDONLY);
	if (fd < 0) { perror(path); goto _throw; }
	if ((read(fd, s, size)) < 0) { perror("read"); goto _throw; }
	close(fd); /* There's no point checking close for errors */

	return s;

_throw:
	if (fd > 0) { close(fd); }
	if (s) free(s);
	return NULL;
}


static void init_state_machine(void)
{
	/* Set up equivalence classes */
	size_t i = 0;
	for (i = 0; i < ASCIIMAX; i++) {
		equclass[i] = EQ_BYTE;
	}

	equclass['\0'] = EQ_END;
	equclass['\n'] = EQ_NEWLINE;

	/* Treating \r as whitespace is a bit of a hack, yes.
	 * We *can* create more complex machinary to cope with it but,
	 * it's really outside the scope of this, to be quite honest */
	equclass[' '] = equclass['\r'] = equclass['\t'] = EQ_WSPACE;

	/* Translate equivalence classes to state transitions */
	/* S_END is also S_START, which has some nice properties, but you
	 * probably wouldn't want to do this with more complex machines */
	states[S_END][EQ_END]     = S_END;
	states[S_END][EQ_BYTE]    = S_BYTE;
	states[S_END][EQ_NEWLINE] = S_SPACE;
	states[S_END][EQ_WSPACE]  = S_SPACE;

	states[S_BYTE][EQ_END]     = S_END;
	states[S_BYTE][EQ_BYTE]    = S_BYTE;
	states[S_BYTE][EQ_NEWLINE] = S_ISWORD;
	states[S_BYTE][EQ_WSPACE]  = S_ISWORD;

	states[S_ISWORD][EQ_END]     = S_END;
	states[S_ISWORD][EQ_BYTE]    = S_BYTE;
	states[S_ISWORD][EQ_NEWLINE] = S_SPACE;
	states[S_ISWORD][EQ_WSPACE]  = S_SPACE;
	
	states[S_SPACE][EQ_END]     = S_END;
	states[S_SPACE][EQ_BYTE]    = S_BYTE;
	states[S_SPACE][EQ_NEWLINE] = S_SPACE;
	states[S_SPACE][EQ_WSPACE]  = S_SPACE;


	count_newlines[EQ_NEWLINE] = 1;

	count_words[S_ISWORD] = 1;

	count_bytes[S_BYTE] = count_bytes[S_ISWORD] = count_bytes[S_SPACE] = 1;
}


static int summarize_file(file_stats *stats)
{
	uchar *s = NULL;
	int state = 0;

	if (!stats || !stats->s) { return 0; }

	s = stats->s;
	stats->bytes = stats->newlines = stats->words = 0;

	do {
		int equ = equclass[*s++];
		state = states[state][equ];

		stats->newlines += count_newlines[equ];
		stats->bytes    += count_bytes[state];
		stats->words    += count_words[state];
	} while (state > S_END);

	return 1;
}


static ssize_t opts(char **argv, size_t *filearg_index, int *optflag)
{
	size_t i = 1;
	ssize_t n = 0;

	if (!argv[i] || strlen(argv[i]) == 0) { return -1; }

	if (LONGOPT("-h", "--help", argv[i])) { return -1; }
	else if (LONGOPT("-c", "--bytes", argv[i])) { *optflag = RESULTS_BYTES; i++; }
	else if (LONGOPT("-w", "--words", argv[i])) { *optflag = RESULTS_WORDS; i++; }
	else if (LONGOPT("-l", "--lines", argv[i])) { *optflag = RESULTS_LINES; i++; }

	*filearg_index = i;
	
	for (; argv[i]; i++) {
		n++;
	}

	return n;
}


static void print_file_stats(int optflag, char *filename, file_stats *stats)
{
	char result_fmtstr[] = "   %lu %s\n";

	if (!filename) { return; }

	if (optflag == RESULTS_ALL) {
		printf("%lu %lu %lu %s\n",
		       stats->newlines, stats->words, stats->bytes, filename);
	}
	else if (optflag == RESULTS_BYTES) {
		printf(result_fmtstr, stats->bytes, filename);
	}
	else if (optflag == RESULTS_LINES) {
		printf(result_fmtstr, stats->newlines, filename);
	}
	else if (optflag == RESULTS_WORDS) {
		printf(result_fmtstr, stats->words, filename);
	}
	
}


static void clean_stats(file_stats *stats, size_t num_stats)
{
	size_t i = 0;

	if (!stats || num_stats == 0) { return; }

	for (i = 0; i < num_stats; i++) {
		if (stats[i].s) { free(stats[i].s); }
	}
	free(stats);
}


int main(int argc, char **argv)
{
	file_stats *stats = NULL;
	file_stats total = { NULL };
	ssize_t num_stats = 0;
	size_t filearg_index = 0;
	int optflags = RESULTS_ALL;

	ssize_t i = 0;

	if (argc == 1 || (num_stats = opts(argv, &filearg_index, &optflags)) < 0) {
		printf("Bad / Insufficient arguments.\n");
		printf(HELPSTRING);
		goto _throw;
	}
	if (num_stats < 1 || !filearg_index) {
		printf("No files provided!\n");
		printf(HELPSTRING);
		goto _throw;
	}

	if (!(stats = calloc(num_stats, sizeof(file_stats)))) {
		perror("calloc");
		goto _throw;
	}

	init_state_machine();

	for (i = 0; i < num_stats; i++) {
		int arg_index = filearg_index + i;

		if (!(stats[i].s = readbuffer(argv[arg_index]))) {
			goto _throw;
		}

		if (!summarize_file(&stats[i])) { goto _throw; }
		
		total.bytes += stats[i].bytes;
		total.words += stats[i].words;
		total.newlines += stats[i].newlines;

		print_file_stats(optflags, argv[arg_index], &stats[i]);
	}

	if (num_stats > 1) {
		print_file_stats(optflags, "total", &total);
	}

	clean_stats(stats, num_stats);
	return 0;
_throw:
	clean_stats(stats, num_stats);
	return 1;
}
