#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define uchar unsigned char
#define ulong unsigned long
#define LOPT(so, lo, arg) ((strcmp(so, arg)) == 0 || (strcmp(lo, arg)) == 0)
#define ASCIIMAX 256

#define HELPSTRING "twc -- tiny word count\n" \
                   "Usage: twc OPTION <files ...>\n" \
                   "Options:\n" \
                   "\t-c, --bytes\n\t\t\tPrint the character/byte count\n" \
                   "\t-w, --words\n\t\t\tPrint the word count\n" \
                   "\t-l, --lines\n\t\t\tPrint the line count\n"

enum { EQ_END, EQ_BYTE, EQ_NEWLINE, EQ_WSPACE, EQ_NCLASSES };
enum { S_END, S_BYTE, S_ISWORD, S_SPACE, S_NSTATES };

struct fstats {
	uchar *s;
	ulong bytes, words, newlines;
};

int equclass[ASCIIMAX];
int states[S_NSTATES][EQ_NCLASSES];

int cntnl[EQ_NCLASSES];
int cntbt[S_NSTATES];
int cntwd[S_NSTATES];

enum { R_ALL, R_BYTES, R_WORDS, R_LINES };
char *resstrs[2] = { "%lu %lu %lu %s\n", "   %lu %s\n" };


static uchar *readbuf(char *path)
{
	struct stat st;
	uchar *s = NULL;
	int fd = -1;
	size_t size = 0;

	/* get data about file (including size in bytes) */
	if ((stat(path, &st)) < 0) { perror(path); goto _throw; }
	
	size = st.st_size;
	if (size == 0) { printf("%s is empty.\n", path); goto _throw; }

	/* The size of a char is always one */
	s = calloc(size, 1);
	if (!s) { perror("calloc"); goto _throw; }

	fd = open(path, O_RDONLY);
	if (fd < 0) { perror(path); goto _throw; }
	if ((read(fd, s, size)) < 0) { perror("read"); goto _throw; }
	close(fd); /* There's no point checking close for errors */
	
	return s;

_throw:
	if (fd > 0) { close(fd); }
	if (s) free(s);
	return NULL;
}


static int init_sm(void)
{
	/* Set up equivalence classes */
	size_t i = 0;
	for (i = 0; i < ASCIIMAX; i++)
		equclass[i] = EQ_BYTE;
	equclass['\0'] = EQ_END;
	equclass['\n'] = EQ_NEWLINE;
	equclass[' '] = equclass['\r'] = equclass['\t'] = EQ_WSPACE;

	/* Set up state automata */
	states[S_END][EQ_END] = S_END;
	states[S_END][EQ_BYTE] = S_BYTE;
	states[S_END][EQ_NEWLINE] = S_SPACE;
	states[S_END][EQ_WSPACE] = S_SPACE;

	states[S_BYTE][EQ_END] = S_END;
	states[S_BYTE][EQ_BYTE] = S_BYTE;
	states[S_BYTE][EQ_NEWLINE] = S_ISWORD;
	states[S_BYTE][EQ_WSPACE] = S_ISWORD;

	states[S_ISWORD][EQ_END] = S_END;
	states[S_ISWORD][EQ_BYTE] = S_BYTE;
	states[S_ISWORD][EQ_NEWLINE] = S_SPACE;
	states[S_ISWORD][EQ_WSPACE] = S_SPACE;
	
	states[S_SPACE][EQ_END] = S_END;
	states[S_SPACE][EQ_BYTE] = S_BYTE;
	states[S_SPACE][EQ_NEWLINE] = S_SPACE;
	states[S_SPACE][EQ_WSPACE] = S_SPACE;


	cntnl[EQ_BYTE] = 0; cntnl[EQ_WSPACE] = 0; cntnl[EQ_NEWLINE] = 1;
	cntbt[S_BYTE] = 1; cntbt[S_ISWORD] = 1; cntbt[S_SPACE] = 1;
	cntwd[S_BYTE] = 0; cntwd[S_ISWORD] = 1; cntwd[S_SPACE] = 0;
	return 1;
}


static int count_file(struct fstats *stats)
{
	uchar *s = NULL;
	int state;

	if (!stats || !stats->s) { return 0; }
	s = stats->s;
	stats->bytes = stats->newlines = stats->words = state = 0;
	do {
		int equ = equclass[*s++];
		state = states[state][equ];

		stats->newlines += cntnl[equ];
		stats->bytes += cntbt[state];
		stats->words += cntwd[state];
	} while (state > S_END);
	return 1;
}

static ssize_t opts(char **argv, size_t *fileix, int *opt_flag)
{
	size_t i = 1;
	ssize_t n = 0;
	if (!argv[i] || strlen(argv[i]) == 0) { return -1; }

	if (LOPT("-h", "--help", argv[i])) {  return -1; }
	else if (LOPT("-c", "--bytes", argv[i])) { *opt_flag = R_BYTES; i++; }
	else if (LOPT("-w", "--words", argv[i])) { *opt_flag = R_WORDS; i++; }
	else if (LOPT("-l", "--lines", argv[i])) { *opt_flag = R_LINES; i++; }

	*fileix = i;
	
	for (; argv[i]; i++)
		n++;

	return n;
}

static void print_fstat(int optflag, char *filename, struct fstats *stats)
{
	char *fmtstr = (optflag > R_ALL ? resstrs[1] : resstrs[R_ALL]);
	if (!filename) { return; }

	if (optflag == R_ALL) {
		printf(fmtstr,
		       stats->newlines, stats->words, stats->bytes, filename);
	}
	else if (optflag == R_BYTES) {
		printf(fmtstr, stats->bytes, filename);
	}
	else if (optflag == R_LINES) {
		printf(fmtstr, stats->newlines, filename);
	}
	else if (optflag == R_WORDS) {
		printf(fmtstr, stats->words, filename);
	}

}


static void clean_stats(struct fstats *stats, size_t nstats)
{
	if (stats && nstats > 0) {
		size_t i = 0;
		for (i = 0; i < nstats; i++) {
			if (stats[i].s) free(stats[i].s);
		}
		free(stats);
	}
}
	

int main(int argc, char **argv)
{
	struct fstats *stats = NULL;
	struct fstats total;
	ssize_t nstats = 0; size_t filesix = 0;
	int opt_flags = R_ALL;
	ssize_t i = 0;

	if (argc == 1 || (nstats = opts(argv, &filesix, &opt_flags)) <= 0) {
		printf("Bad / Insufficient arguments.\n");
		printf(HELPSTRING);
		goto _throw;
	}
	if (nstats < 1 || !filesix) { printf("Fatal error\n"); goto _throw; }

	memset(&total, 0, sizeof(struct fstats));

	stats = calloc(nstats, sizeof(struct fstats));
	if (!stats) { perror("calloc"); goto _throw; }

	init_sm();

	for (i = 0; i < nstats; i++) {
		if (!(stats[i].s = readbuf(argv[i+filesix]))) { goto _throw; }
		if (!count_file(&stats[i])) { goto _throw; }
		total.bytes += stats[i].bytes;
		total.words += stats[i].words;
		total.newlines += stats[i].newlines;
		print_fstat(opt_flags, argv[i+filesix], &stats[i]);
	}

	if (nstats > 1) { print_fstat(opt_flags, "total", &total); }
	clean_stats(stats, nstats);
	return 0;

_throw:
	clean_stats(stats, nstats);
	return 1;
}
